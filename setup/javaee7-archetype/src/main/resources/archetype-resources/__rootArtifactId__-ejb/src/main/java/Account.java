#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import java.io.InputStream;
import javax.ejb.Local;

/**
 * A sample stateless session bean.
 * 
 */
@Local
public interface Account {

  void registerAccount(String accountName);
  
}
