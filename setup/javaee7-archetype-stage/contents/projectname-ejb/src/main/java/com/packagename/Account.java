package com.packagename;

import java.io.InputStream;
import javax.ejb.Local;

/**
 * A sample stateless session bean.
 * 
 */
@Local
public interface Account {

  void registerAccount(String accountName);
  
}
