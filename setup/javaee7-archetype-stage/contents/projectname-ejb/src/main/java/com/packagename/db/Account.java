/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.packagename.db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author safi
 */
@Entity
public class Account implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long accountId;
  
  protected String accountName;

  public Long getAccountId() {
    return accountId;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }
  
  @Override
  public int hashCode() {
    int hash = 17;
    hash = 31 * hash + (accountId != null ? accountId.hashCode() : 0);
    hash = 31 * hash + (accountName != null ? accountName.hashCode(): 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    if (object == this) {
      return true;
    }
    // TODO: Warning - this method won't work in the case the accountId fields are not set
    if (!(object instanceof Account)) {
      return false;
    }
    Account other = (Account) object;
    if ((this.accountId == null && other.accountId != null) || (this.accountId != null && !this.accountId.equals(other.accountId))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return String.format("%s[ id=%s ]", this.getClass().getName(), accountId);
  }
  
}
