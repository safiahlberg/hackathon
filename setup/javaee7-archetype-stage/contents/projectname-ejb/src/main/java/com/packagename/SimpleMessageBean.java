package com.packagename;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A sample message driven bean.
 * 
 * It accepts a message of type {@link TextMessage}
 * 
 */
@MessageDriven(
activationConfig = {
  @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
  @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/queue/aq")
})
public class SimpleMessageBean implements MessageListener {

  static final Logger LOGGER = LoggerFactory.getLogger(SimpleMessageBean.class);
  
  @Resource
  public MessageDrivenContext mdc;

  @Override
  public void onMessage(Message inMessage) {
    TextMessage msg;

    try {
      if (inMessage instanceof TextMessage) {
        msg = (TextMessage) inMessage;

        LOGGER.info(msg.getText());        
      } else {
        LOGGER.error("Wrong message type or null message: %s", inMessage.getClass());
      }
    } catch (JMSException e) {
      LOGGER.error(
              "MessageBean.onMessage: JMSException: %s", e.getMessage(), e);
      // e.printStackTrace();
      mdc.setRollbackOnly();
    } catch (Throwable te) {
      LOGGER.error("MessageBean.onMessage: Exception: %s", te.toString(), te);
      // te.printStackTrace();
    }
  }
}
