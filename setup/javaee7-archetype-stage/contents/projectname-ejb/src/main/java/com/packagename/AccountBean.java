package com.packagename;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation for the sample stateless session bean.
 *
 */
@Stateless
public class AccountBean implements Account {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(AccountBean.class);

  private Connection connection;
  @Resource
  SessionContext sessionContext;
  @Resource(lookup = "jms/ConnectionFactory")
  private ConnectionFactory connectionFactory;
  @Resource(lookup = "jms/queue/aq")
  private Queue queue;

  @PostConstruct
  public void makeConnection() {
    try {
      connection = connectionFactory.createConnection();
    } catch (JMSException ex) {
      LOGGER.error("Could not make connection", ex);
    }
  }

  @PreDestroy
  public void endConnection() {
    if (connection != null) {
      try {
        connection.close();
      } catch (JMSException ex) {
        LOGGER.error("Could not end connection", ex);
      }

    }
  }

  @Override
  public void registerAccount(String accountName) {
    Session session = null;
    TextMessage message;
    MessageProducer publisher;

    try {
      session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

      message = session.createTextMessage(accountName);

      publisher = session.createProducer(queue);

      publisher.send(message);
    } catch (JMSException t) {
      LOGGER.error("Error when sending message", t);
      sessionContext.setRollbackOnly();
      throw new RuntimeException(t);
    } finally {
      if (session != null) {
        try {
          session.close();
        } catch (JMSException ex) {
          LOGGER.error("Error when closing session", ex);
        }
      }
    }
  }
}
